/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import com.celestial.dsb.core.db.LinkDAO;

import domain.Bookmark;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Selvyn
 */
public class URLController
{
    private final TagManipulator itsTagManipulator = new TagManipulator();
    private final LinkDAO itsLinkdao = new LinkDAO();

    public void saveURL(String url, String description, String tags)
    {
        String[] separatedTags = itsTagManipulator.parseString(tags, ",\\ *");

        ArrayList<String> tagsToSave = new ArrayList(Arrays.asList(separatedTags));

        description = description.replace("'", ""); // remove ' character, causes SQL exceptions

        try
        {
            itsLinkdao.saveLink(url, description, tagsToSave);
        } catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Bookmark> getUrlForTag(String tag)
    {
        ArrayList<Bookmark> bookmarks = itsLinkdao.getLinksForTag(tag);

        MetatagController mc = new MetatagController();
        for (Bookmark bm : bookmarks)
        {
            bm.setTags(mc.getTagsForLink(bm.getId()));
        }

        return bookmarks;
    }

    public ArrayList<Bookmark> getAllLinks()
    {
        ArrayList<Bookmark> bookmarks = itsLinkdao.getAllLinks();

        MetatagController mc = new MetatagController();
        for (Bookmark bm : bookmarks)
        {
            bm.setTags(mc.getTagsForLink(bm.getId()));
        }

        return bookmarks;
    }
}
