/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import java.util.regex.Pattern;

/**
 *
 * @author Selvyn
 */
public class TagManipulator
{

    public TagManipulator()
    {
    }

    String[] parseString(String tags, String regex)
    {
        String result[] = null;

        Pattern pp = Pattern.compile(regex);
        result = pp.split(tags.trim());

        return result;
    }
}
