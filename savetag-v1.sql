CREATE DEFINER=`root`@`%` PROCEDURE `saveTag`( in tag varchar(45))
BEGIN
	if not exists( select 1 from metatag where tag_name = tag ) then
		insert into metatag values ( tag );
	end if;
END